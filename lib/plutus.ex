defmodule Plutus do
  @moduledoc """
  Documentation for `Plutus`.
  """

  @doc """
  Hello world.

  ## Examples

      iex> Plutus.hello()
      :world

  """
  def hello do
    :world
  end
end
